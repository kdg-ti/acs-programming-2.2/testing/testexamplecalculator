package be.kdg.pro4.calculator;


import org.junit.jupiter.api.*;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


public class HamcrestTests {
  private Calculator calculator;

  @BeforeEach
  public void setUp() {
    // Given (arrange)
    calculator = new Calculator();
  }

  @Test
  public void addTwoPositiveIntsShouldGiveResult() {
    // Given (arrange)
    int nr1 = 10;
    int nr2 = 50;
    // When (Act)
    // Then (Assert)
    assertThat(String.format("The sum of %d and %d should be %1f.", nr1, nr2, 60.0),
        calculator.add(nr1, nr2),
        is(60.0));
  }

  @Test
  public void multiplyTwoNegativeIntsShouldBePositive() {
    assertThat("The product of 2 negative numbers should not be negative.",
        calculator.multiply(-10, -50),
        greaterThanOrEqualTo(0.0));
  }

  @Test
  @DisplayName("Given a list of ints, then they all should be positive")
  public void everyItemInListShouldBePositive() {
    assertThat(List.of(1, 8, 455), everyItem(greaterThan(0)));
  }


  @Test
  @DisplayName("Given a list of ints, then it should include 455 and 8 ")
  public void listShouldHaveItems() {
    assertThat(List.of(1, 8, 455), hasItems(455, 8));

  }

  @DisplayName("Given a list of ints, then it shoud consist of 1,  455 and 8 (unordered)")
  public void listShouldConsistOfItemsInAnyOrder() {
    assertThat(List.of(1, 8, 455), containsInAnyOrder(455, 1, 8));
  }


}

