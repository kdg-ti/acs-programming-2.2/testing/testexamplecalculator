package be.kdg.pro4.calculator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


public class CalculatorTest {
	private Calculator calculator;

	@BeforeEach
	public void setUp() {
		// Given (arrange)
		calculator = new Calculator();
	}

	@AfterEach
	public void tearDown() {
		System.out.println("Another test done!");
	}

	@Test
	public void addTwoPositiveIntsShouldBeCorrect() {
		// Given (arrange)
		int nr1 = 10;
		int nr2 = 50;
		// When (Act)
		double result = calculator.add(nr1, nr2);
		// Then (Assert)
		int expected = 60;
		assertEquals(60,  result,
			() -> String.format("The sum of %d and %d should be %d.", nr1, nr2, expected));
	}

	@Test
	public void multiplyTwoPositiveIntsShouldBeCorrect() {
		// When
		double result = calculator.multiply(10, 50);
		// Then
		assertEquals(500, result, "The product of 10 and 50 should be <500>.");
	}

	@Test
	public void multiplyTwoNegativeIntsShouldBePositive() {
		// Given (arrange)
		int nr1 = 10;
		int nr2 = 50;
		// When (Act)
		double result = calculator.multiply(-10, -50);
		// Then
		assertTrue(result >= 0,"The product of 2 negative numbers should not be negative.");
	}

	@Test
	@DisplayName("Given two ints, when dividing, then result should be a truncated int")
	public void dividePositiveIntsShouldBeInt() {
		// When, Then
		assertEquals(3, calculator.divide(10, 3), "10/3 should be 3.");
	}

	@Test
	@DisplayName("Given two numbers and at least one is a double, when dividing, then the result should be a double (deviation of 0.01 allowed)")
	public void divideWithDoublesShouldBeDouble() {
		// When, Then
		assertEquals(3.33, calculator.divide(10, 3.0), 0.01,"10/3 should be 3.33");
	}

	@Test
	@DisplayName("Given an int and a divisor of 0, when dividing, then a Runtime ArithmeticException should be thrown")
	public void divideIntByZeroShouldThrowException() {
		// When, Then
		assertThrows(ArithmeticException.class,
			() -> calculator.divide(10, 0),
			"Divide by 0 should throw exception!");
	}

	@Test
	@DisplayName("Given two numbers and at least one is a double , when dividing by zero, then the result should be infinity")
	public void divideDoubleByZeroShouldBeInfinity() {
		// When, Then
		assertTrue(Double.isInfinite(calculator.divide(10, 0.0)),"Dividing by 0.0 should return infinity");
		assertTrue(Double.isInfinite(calculator.divide(10.0, 0)),"Dividing 10.0 by 0 should return infinity");
	}


}
